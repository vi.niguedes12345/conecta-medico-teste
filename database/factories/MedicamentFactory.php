<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Medicament::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'presentation' => $faker->text,
        'cod_ean' => $faker->unique()->ean13,
        'quantity' => $faker->randomDigit,
        'dose' => $faker->regexify('[A-Za-z0-9]{20}'),
        'toxicity_alert' => $faker->boolean,
    ];
});
