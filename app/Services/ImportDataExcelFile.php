<?php namespace App\Services;

use App\Repositories\MedicamentRepositories\MedicamentRepositoryInterface;
use Maatwebsite\Excel\Facades\Excel;

use Exception;
use Illuminate\Support\Facades\Validator;

class ImportDataExcelFile
{
    protected $medicament;
    public function __construct(MedicamentRepositoryInterface $medicamentRepositoryInterface)
    {
        $this->medicament = $medicamentRepositoryInterface;
    }

    public function saveDataImport($file)
    {
        try { 
            Excel::load($file->file('file')->getRealPath(), function ($reader){
                foreach ($reader->toArray() as $key => $row) 
                {
                    if($this->validateFields($row))
                    {
                        $this->medicament->updateByEanOrCreate($row);
                    }
                }
            });

            return true;
        }catch(Exception $exception){
            \Log::error($exception);
            return false;  
        }
    }

    public function validateFields($row)
    {
        $existDataRow = !empty($row);
        $existEssencialsFields = !empty($row['nome']) && !empty($row['ean']) && !empty($row['dose']);
        if($existDataRow && $existEssencialsFields)
            return true;

        return false;
    }
}
