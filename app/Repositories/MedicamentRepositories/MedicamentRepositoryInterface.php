<?php

namespace App\Repositories\MedicamentRepositories;

interface MedicamentRepositoryInterface
{
    public function paginate($pages);
    
    public function find($id);

    public function updateByEanOrCreate($data);

    public function findBy($value, $column);

    public function notNull($column);

    public function update($id, $data);

    public function destroy($id);

    public function orderBy($column, $orderby);
}

?>