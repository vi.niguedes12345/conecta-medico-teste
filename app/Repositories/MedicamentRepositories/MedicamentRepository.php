<?php

namespace App\Repositories\MedicamentRepositories;

use App\Models\Medicament;
use App\Repositories\MedicamentRepositories\MedicamentRepositoryInterface;

class MedicamentRepository implements MedicamentRepositoryInterface
{
    protected $medicament;
    public function __construct(Medicament $medicament)
    {
        $this->medicament =  $medicament;
    }

    public function paginate($pages)
	{
		return $this->medicament->paginate($pages);
    } 

    public function find($id)
    {
        return $this->medicament->find($id);
    }
    
    public function findBy($column, $field)
    {
        return $this->medicament->where($column, $field)->first();
    }

    public function updateByEanOrCreate($data)
    {
        return $this->medicament->updateOrCreate([
            'cod_ean' => $data['ean']
        ],[
            'name' => $data['nome'],
            'presentation' => $data['apresentacao'],
            'cod_ean' => $data['ean'],
            'quantity' => $data['quantidade'],
            'dose' => $data['dose'],
            'toxicity_alert' => $data['alerta_de_toxicadade'],
        ]);
    }

    public function notNull($column)
    {
        return $this->medicament->whereNotNull($column)->get();
    }

    public function update($id, $data)
    {
        $this->medicament->where('id',$id)->update([ 
            'name' => $data['name'],
            'presentation' => $data['presentation'],
            'cod_ean' => $data['cod_ean'],
            'quantity' => $data['quantity'],
            'dose' => $data['dose'],
            'toxicity_alert' => $data['toxicity_alert'],
        ]);
    }

    public function destroy($id)
    {
        return $this->medicament->destroy($id);
    }

    public function orderBy($column, $orderBy)
    {
        return $this->medicament->orderBy($column, $orderBy);
    }
}
?>
