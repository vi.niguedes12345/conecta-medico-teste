<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\MedicamentRepositories\MedicamentRepositoryInterface;

class MedicamentsController extends Controller
{
    CONST PRESENTATION = 'presentation';
    CONST TOXICITY_ALERT = 'toxicity_alert';
    CONST COD_EAN = 'cod_ean';

    protected $medicament;
    public function __construct(MedicamentRepositoryInterface $medicamentRepositoryInterface)
    {
        $this->medicament = $medicamentRepositoryInterface;
    }

    public function listMedicamentsByPresentation($orderBy)
    {
        $medicament = $this->medicament->orderBy(self::PRESENTATION, $orderBy);
        if(empty($medicament))
            return response(null, 404);
        
        return response()->json($medicament, 200);
    }

    public function listMedicamentsWithToxicityAlert()
    {
        $medicament = $this->medicament->notNull(self::TOXICITY_ALERT);
        if(empty($medicament))
            return response(null, 404);
        
        return response()->json($medicament, 200);
    }

    public function getMedicamentByID($id)
    {
        $medicament = $this->medicament->find($id);
        if(empty($medicament))
            return response(null, 404);
        
        return response()->json($medicament, 200);
    }

    public function getMedicamentByEan($ean)
    {
        $medicament = $this->medicament->findBy(self::COD_EAN, $ean);
        if(empty($medicament))
            return response(null, 404);
        
        return response()->json($medicament, 200);
    }
}
