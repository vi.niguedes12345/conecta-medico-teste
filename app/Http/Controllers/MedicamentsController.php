<?php

namespace App\Http\Controllers;

use App\Repositories\MedicamentRepositories\MedicamentRepositoryInterface;
use App\Services\ImportDataExcelFile;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class MedicamentsController extends Controller
{
    protected $medicament;
    public function __construct(MedicamentRepositoryInterface $medicamentRepositoryInterface)
    {
        $this->medicament = $medicamentRepositoryInterface;
    }

    public function show()
    {
        $medicaments =  $this->medicament->paginate(10);
        return view('pages.medicaments.show', compact('medicaments'));
    }

    public function create()
    {
        return view('pages.medicaments.create');
    }

    public function save(Request $request)
    {
        if(!$request->hasFile('file'))
            return redirect()->back()->withErrors('Por favor insira um arquivo para importar!');

        $dataFile = App::make(ImportDataExcelFile::class);
        if(!$dataFile->saveDataImport($request))
            return redirect()->back()->withErrors('Erro no arquivo, verifique o arquivo de importação!');

        return redirect()->route("medicament.index")->with('success','Medicamentos importados com sucesso!');
    }

    public function edit($id)
    {
        $medicament = $this->medicament->find($id);
        if(empty($medicament))
            return redirect()->route("medicament.index")->withErrors('Esse registro que está tentando acessar não existe!');

        return view('pages.medicaments.edit', compact('medicament'));
    }

    public function update(Request $request, $id)
    {
        try{
            $data = $request->except(['_token']);
            if(empty($data))
                return false;
    
            $this->medicament->update($id,$data);
            return redirect()->route("medicament.index")->with('success','Editado com sucesso!');

        }catch (\Illuminate\Database\QueryException $e) {
            \Log::error($e);
            return redirect()->back()->withErrors('Não conseguimos atualizar os dados, verifique todos os campos!');

        } catch (\Exception $e) {
            \Log::error($e);
            return redirect()->back()->withErrors('Algo inesperado aconteceu, tente novamente ou contate o suporte!');
        }
       
    }

    public function destroy($id)
    {
        if(!$this->medicament->find($id))
            return redirect()->back()->withErrors('Não conseguimos encontrar o registro!');
       
        $this->medicament->destroy($id);
        return redirect()->route("medicament.index")->with('success','Deletado com sucesso!');
    }
}
