@csrf
    <div class="mb-3 col-md-6">
        <label class="form-label">Nome</label>
        <input 
            type="text" 
            class="form-control" 
            value="{{  !empty($medicament->name) ? $medicament->name : null }}" 
            name="name"
            maxlength="150"  
            placeholder="Nome"
        >
    </div>

    <div class="mb-3 col-md-6">
        <label class="form-label">Apresentação</label>
        <input 
            type="text" 
            class="form-control" 
            value="{{  !empty($medicament->presentation) ? $medicament->presentation : null }}" 
            name="presentation"
            maxlength="220" 
            placeholder="Apresentação"
        >
    </div>

    <div class="mb-3 col-md-6">
        <label class="form-label">Código EAN</label>
        <input 
            type="number" 
            class="form-control" 
            value="{{  !empty($medicament->cod_ean) ? $medicament->cod_ean : null }}" 
            name="cod_ean" 
            placeholder="Código EAN"
        >
    </div>

    <div class="mb-3 col-md-6">
        <label class="form-label">Quantidade</label>
        <input 
            type="number" 
            class="form-control" 
            value="{{  !empty($medicament->quantity) ? $medicament->quantity : null }}" 
            name="quantity" 
            placeholder="Quantidade"
        >
    </div>

    <div class="mb-3 col-md-6">
        <label class="form-label">Dose</label>
        <input 
            type="text" 
            class="form-control" 
            value="{{  !empty($medicament->dose) ? $medicament->dose : null }}" 
            name="dose"
            maxlength="30"  
            placeholder="Dose"
        >
    </div>

    <div class="mb-3 col-md-6">
        <label class="form-label">Alerta de Tóxidade</label>
        <input 
            type="text" 
            class="form-control" 
            value="{{  !empty($medicament->toxicity_alert) ? $medicament->toxicity_alert : null }}" 
            name="toxicity_alert"
            maxlength="100"  
            placeholder="Alerta de Tóxidade"
        >
    </div>
    <br>