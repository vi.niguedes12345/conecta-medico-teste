@extends('layouts.app')

@section('content')
<div class="container">
    @if($errors->any())
        <div class="alert alert-danger">{{$errors->first()}}</div>
        <br><br>
    @endif
    <div class="row justify-content-center">
        <form class="form" action="{{ route('medicament.update', ['id' => $medicament->id]) }}" method="post">
            <div class="row">
                @include('pages.medicaments._form')
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success">Editar</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection