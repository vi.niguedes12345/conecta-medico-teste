@extends('layouts.app')

@section('content')
<div class="container">       
    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        <br><br>
    @endif
    @if($errors->any())
        <div class="alert alert-danger">{{$errors->first()}}</div>
        <br><br>
    @endif
    <a class="btn btn-success" role="button" href="{{ route('medicament.create') }}">Novos medicamentos</a>
    <br><br>
    <h3>Medicamentos</h3>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" style="background-color: white">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Apresentação</th>
                                <th scope="col">EAN</th>
                                <th scope="col">Quantidade</th>
                                <th scope="col">Dose</th>
                                <th scope="col">Alerta de Tóxidade</th>
                                <th scope="col">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($medicaments as $medicament)
                            <tr>
                                <th scope="row">
                                    {{  
                                        !empty($medicament->id) ? $medicament->id : ''
                                    }}
                                </th>
                                <td>
                                    {{  
                                        !empty($medicament->name) ? $medicament->name : ''
                                    }}
                                </td>
                                <td>
                                    {{  
                                        !empty($medicament->presentation) ? $medicament->presentation : ''
                                    }}
                                </td>
                                <td>
                                    {{  
                                        !empty($medicament->cod_ean) ? $medicament->cod_ean : ''
                                    }}
                                </td>
                                <td>
                                    {{  
                                        !empty($medicament->quantity) ? $medicament->quantity : ''
                                    }}
                                </td>
                                <td>
                                    {{  
                                        !empty($medicament->dose) ? $medicament->dose : ''
                                    }}
                                </td>
                                <td>
                                    {{  
                                        !empty($medicament->toxicity_alert) ? $medicament->toxicity_alert : ''
                                    }}
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-success" role="button" href="{{route('medicament.edit', ['id' => $medicament->id])}}">Editar</a>
                                        <a class="btn btn-danger" role="button" href="{{route('medicament.delete', ['id' => $medicament->id])}}">Deletar</a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="pull-right">

                    {{ $medicaments->render() }}
            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection