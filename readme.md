# Teste - Conecta Médico
## _Importador de medicamentos_


Desenvolver ferramenta para controle de estoque de medicamentos que importe um arquivo XLS ou CSV através de um formulário e disponibilize essas informações em uma API REST.

- O arquivo deve conter as colunas Nome, Apresentação, EAN, Quantidade, Dose usual/padrão, Alerta de toxicidade;
- Não poderão existir medicamentos com EAN repetidos;
- O usuário deve poder visualizar, editar e excluir os dados que foram importados por um painel, mas não deverá poder cadastrar novos medicamentos manualmente;
- Em caso de importação de nova tabela de medicamentos, caso já exista um medicamento cadastrado com o mesmo EAN, este deve ser atualizado;
- Deve ser possível consultar os medicamentos via API utilizando métodos que possibilitem
Listar medicamentos por apresentação;
Listar medicamentos com alerta de toxicidade;
Retornar medicamento por ID;
Retornar medicamento por EAN


## Para rodar este projeto

```bash
$ git clone https://gitlab.com/vi.niguedes12345/conecta-medico-teste
$ cd conecta-medico-teste
$ composer install
$ cp .env.example .env
$ php artisan key:generate
$ php artisan migrate #antes de rodar este comando verifique sua configuracao com banco em .env
$ php artisan serve
$ php artisan db:seed #para gerar os seeders, dados de teste
```
## Como testar
Como utilizei o Auth do laravel para facilitar, após fazer o passo a passo acima teremos a url **localhost:8000** e para acessar a rota de [medicamentos][RouteMedicament], precisaremos logar antes, e o email e senha se encontra dentro de **UsersTableSeeder** abaixo os arquivos usados para teste de importação dos medicamentos.


## Arquivo .csv e .xlsx

| Arquivo |
| ------ |
| [medicamentos.csv][PlGdCsv] |
| [medicamentos.xlsx][PlGdXlsx] |

## Testes Unitários - Opcional
Criei um arquivo /tests/Unit/MedicamentTest onde faço 4 testes apenas das rotas de api 
- Retornar medicamento por ID;
- Retornar medicamento por EAN;

para testá-lo, precisa primeiro rodar a mgiration e depois é só rodar no terminal o seguinte comando: **_vendor/bin/phpunit_**


   [PlGdCsv]: <https://drive.google.com/file/d/1wbvoEBqvE6tJtGDAcMotDEOLamy30qpb/view?usp=sharing>
   [PlGdXlsx]: <https://drive.google.com/file/d/18vLwIVZNlqFkCOLrHHVjM2yqJl6U9-9D/view?usp=sharing>
   [RouteMedicament]: <http://localhost:8000/medicamentos>
  