<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Medicament;

class MedicamentTest extends TestCase
{
    public function testCanShowMedicamentById() 
    {
        $medicament = factory(Medicament::class)->create();
        
        $this->get(route('api.medicament.showById', $medicament->id))
            ->assertStatus(200);
    }

    public function testCanShowMedicamentByCodEan() 
    {
        $medicament = factory(Medicament::class)->create();
        
        $this->get(route('api.medicament.showByEan', $medicament->cod_ean))
            ->assertStatus(200);
    }

    public function testNotFoundMedicamentById() 
    {        
        $this->get(route('api.medicament.showByEan', '10000'))
            ->assertStatus(404);
    }

    public function testNotFoundMedicamentByCodEan() 
    {        
        $this->get(route('api.medicament.showByEan', '10'))
            ->assertStatus(404);
    }
}
