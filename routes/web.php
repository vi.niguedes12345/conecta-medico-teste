<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::group(['prefix' => 'medicamentos'], function () {
        Route::get('/', ['uses' => 'MedicamentsController@show', 'as' => 'medicament.index']);
        Route::get('/cadastrar', ['uses' => 'MedicamentsController@create', 'as' => 'medicament.create']);
        Route::post('/save', ['uses' => 'MedicamentsController@save', 'as' => 'medicament.save']);
        Route::get('/editar/{id}', ['uses' => 'MedicamentsController@edit', 'as' => 'medicament.edit']);
        Route::post('/update/{id}', ['uses' => 'MedicamentsController@update', 'as' => 'medicament.update']);
        Route::get('/deletar/{id}', ['uses' => 'MedicamentsController@destroy', 'as' => 'medicament.delete']);
    });

});


