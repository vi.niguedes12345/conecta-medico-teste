<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'medicamentos'], function () {
    Route::get('/ordenado-por/apresentacao/{orderBy}', ['uses' => 'Api\MedicamentsController@listMedicamentsByPresentation', 'as' => 'api.medicament.create']);
    Route::get('/toxicidade', ['uses' => 'Api\MedicamentsController@listMedicamentsWithToxicityAlert', 'as' => 'api.medicament.listWithToxicity']);
    Route::get('/id/{id}', ['uses' => 'Api\MedicamentsController@getMedicamentByID', 'as' => 'api.medicament.showById']);
    Route::get('/ean/{ean}', ['uses' => 'Api\MedicamentsController@getMedicamentByEan', 'as' => 'api.medicament.showByEan']);
});